package org.manathome.src2doc.core;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.manathome.src2doc.model.ClassDescription;
import org.manathome.src2doc.model.FieldDescription;

public class TemplateWriterTest {

	private TemplateWriter template = new TemplateWriter("templates/ConstantList.peb");

	@Test
	public void testGeneralTemplateGeneration() throws Exception {
				
		List<FieldDescription> fields = new ArrayList<>();
		fields.add(new FieldDescription("aName", "acomment", "3"));
		fields.add(new FieldDescription("login", "a new login value", "/login"));
		fields.add(new FieldDescription("a quoted value", "string contant init", "\"/string in quotes here.\""));
		
		final ClassDescription classDescription = new ClassDescription("sample class", "class comment", fields);
		String markup = template.generate(classDescription, false);
		
		assertThat(markup).contains(".constants from sample class");
		assertThat(markup).contains("| aName ");
		assertThat(markup).contains("| 3 ");
		assertThat(markup).contains("| acomment ");
	}
	
	@Test
	public void testTemplateQuotedFieldGeneration() throws Exception {
		
				
		List<FieldDescription> fields = new ArrayList<>();
		fields.add(new FieldDescription("a quoted value", "string contant init", "\"a string in quotes here.\""));
		
		final ClassDescription classDescription = new ClassDescription("sample class", "class comment", fields);
		String markup = template.generate(classDescription, false);
		
		assertThat(markup).contains("\"a string in quotes here.\"");
	}

	@Test
	public void testTemplateMultilineCommentOnFieldGeneration() throws Exception {
		
				
		List<FieldDescription> fields = new ArrayList<>();
		fields.add(new FieldDescription("mField", "a \n multiline\n comment\n", null));
		
		final ClassDescription classDescription = new ClassDescription("sample class", "class comment", fields);
		String markup = template.generate(classDescription, false);
		
		assertThat(markup).contains("a \n multiline\n comment\n");
	}
	
	@Test
	public void testTemplateWithHeader() throws Exception {
		
				
		List<FieldDescription> fields = new ArrayList<>();
		fields.add(new FieldDescription("mField", "a \n multiline\n comment\n", null));
		
		final ClassDescription classDescription = new ClassDescription("sample class", "class comment", fields);
		String markup = template.generate(classDescription, true);
		
		assertThat(markup).contains("= sample class");
		assertThat(markup).contains("class comment");
	}	

	@Test
	public void testTemplateWithoutHeader() throws Exception {
		
				
		List<FieldDescription> fields = new ArrayList<>();
		fields.add(new FieldDescription("mField", "a \n multiline\n comment\n", null));
		
		final ClassDescription classDescription = new ClassDescription("sample class", "class comment", fields);
		String markup = template.generate(classDescription, false);
		
		assertThat(markup).doesNotContain("= sample class");
	}	

}
