package org.manathome.src2doc.core;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.manathome.src2doc.model.ClassDescription;

public class JavaSourceReaderTest {
	
	private JavaSourceReader reader = new JavaSourceReader();

	private InputStream readFromFile() throws IOException {		
		
		// final var p = Paths.get("D:\\data\\projects\\java\\src2doc\\src\\main\\resources\\sample-source\\MetricName.java.sample");
		final Path p = Paths.get("src/main/resources/sample-source/MetricName.java.sample");
		
		InputStream in = new BufferedInputStream(Files.newInputStream(p));
		return in;
	}
			
	@Test
	public void testParseClass() throws IOException {
		
		final ClassDescription cl = reader.parse(readFromFile());
		assertThat(cl).isNotNull();
		assertThat(cl.name()).contains("MetricName");
	}
	
	@Test
	public void testParseFields() throws IOException {
		
		final ClassDescription cl = reader.parse(readFromFile());
		assertThat(cl.fields().size()).isEqualTo(4);
		assertThat(cl.fields().get(0).name()).contains("login");
		assertThat(cl.fields().get(0).comment()).contains("login");
		assertThat(cl.fields().get(0).value()).contains("login");
	}
	
}
