package org.manathome.src2doc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;

import org.junit.jupiter.api.Test;

public class Src2DocProcessorTest {
	
	private File input = new File("src/main/resources/sample-source/MetricName.java.sample");
	private File output = new File("build/MetricName.java.sample.adoc");

	
    @Test 
    public void processSource2Markup() throws Exception {
    	Src2DocProcessor p = new Src2DocProcessor();
    	
    	assertThat(p.process(input, output, false)).contains(".constants from");
    }

    @Test 
    public void processSource2MarkupWithMissingInput() throws Exception {
    	Src2DocProcessor p = new Src2DocProcessor();
    	
    	assertThrows(NullPointerException.class, 
    			() -> p.process(null, output, false)
    			);
    }
    
}
