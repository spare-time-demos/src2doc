package org.manathome.src2doc;

import static org.assertj.core.api.Assertions.assertThat;  

import org.gradle.testfixtures.ProjectBuilder;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * A simple unit test for the 'org.manathome.src2doc' plugin.
 */
@DisplayName("apply src2doc gradle task")
@Tag("plugin")
public class Src2DocPluginTest {
	
    @Test 
    public void pluginRegistersItsTask() {
        assertThat(
        		getTaskForTest())
        .isNotNull();
    }
    
	
    @Test 
    public void pluginTaskHasInputProperties() {
        assertThat(getTaskForTest()
        		.getInputs()
        		.getHasInputs())
        .isTrue();
    }    

    @Test 
    public void pluginTaskHasOutputsProperties() {
        assertThat(getTaskForTest().getOutputs().getHasOutput()).isTrue();
    }    

    
    private Task getTaskForTest() {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply("org.manathome.src2doc");
        return project.getTasks().findByName("src2docGenerate");
    }
}
