package org.manathome.src2doc;

import org.gradle.api.Project;
import org.gradle.api.Plugin;

/**
 * src2doc plugin: a markup generator gradle plugin.
 */
public class Src2DocPlugin implements Plugin<Project> {

		/** gradle apply. */
	  @Override
		public void apply(Project project) {

			project.getTasks().register(
	    		Src2DocPluginTask.SRC2DOC_TASK_NAME,
	    		Src2DocPluginTask.class);
    }
    
}
