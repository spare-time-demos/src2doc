package org.manathome.src2doc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import org.manathome.src2doc.core.JavaSourceReader;
import org.manathome.src2doc.core.TemplateWriter;
import org.manathome.src2doc.model.ClassDescription;

/** convert java to markup docs. */
public class Src2DocProcessor {

	/** generate output asciidoc from input java file. */
	public String process(File input, File output, boolean withHeaders) throws FileNotFoundException, IOException {
				
		final JavaSourceReader reader = new JavaSourceReader();

		if(input == null) {
			throw new NullPointerException("input file argument null.");
		}

		if(output == null) {
			throw new NullPointerException("output file argument null.");
		}

		if(!input.exists()) {
			throw new IllegalArgumentException("input file not exists: " + input.getPath());
		}

		if(!input.isFile() || ! input.canRead()) {
			throw new IllegalArgumentException("input file not a readable file: " + input.getPath());
		}

		try(InputStream in = new FileInputStream(input)) {
			final ClassDescription cd = reader.parse(in);
			
			final TemplateWriter template = new TemplateWriter("templates/ConstantList.peb");
			final String markup = template.generate(cd, withHeaders);
			
			try(PrintWriter out = new PrintWriter(output)) {
				out.print(markup);
			}
			
			return markup;
		}
	}

}
