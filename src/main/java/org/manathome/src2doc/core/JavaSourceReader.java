package org.manathome.src2doc.core;

import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

import org.manathome.src2doc.model.ClassDescription;
import org.manathome.src2doc.model.FieldDescription;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.TypeDeclaration;

/** parsing java source code. */
public class JavaSourceReader {

	/** parse a class. */
	public ClassDescription parse(final InputStream in) {
		final CompilationUnit compilationUnit = StaticJavaParser.parse(in);

		for(TypeDeclaration<?> aType: compilationUnit.getTypes()) {
			if(aType.isClassOrInterfaceDeclaration()) {
				
				List<FieldDescription >fields =  aType.asClassOrInterfaceDeclaration().getFields()
				.stream()
				.map(fd -> new FieldDescription(
						fd.getVariable(0).getName().asString(), 
						fd.getJavadocComment().isPresent() ? fd.getJavadocComment().get().getContent() : null, 						
					    fd.getVariable(0).getInitializer().isPresent() ? fd.getVariable(0).getInitializer().get().asLiteralStringValueExpr().toString(): null
								))
				.collect(Collectors.toList())
				;
				
				ClassDescription aClass = new ClassDescription(
						aType.getNameAsString(), 
						aType.getJavadocComment().isPresent() ?  aType.getJavadocComment().get().getContent(): null,
						fields);
				
				return aClass;
			}
		}
		return null;		
	}
	
}
