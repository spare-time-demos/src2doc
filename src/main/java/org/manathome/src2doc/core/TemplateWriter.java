package org.manathome.src2doc.core;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import io.pebbletemplates.pebble.PebbleEngine;
import io.pebbletemplates.pebble.template.PebbleTemplate;
import org.manathome.src2doc.model.ClassDescription;

/** use pebble template to generate output from parsed java source. */
public class TemplateWriter {
	
	private final PebbleTemplate template;

	/** .ctor */
	public TemplateWriter(final String templatePath) {
		final PebbleEngine engine = new PebbleEngine.Builder().build();
		this.template = engine.getTemplate(templatePath);		
	}

	/** generate a markup file. */
	public String generate(ClassDescription classDescription, boolean withHeaders) throws IOException {
				
		Writer writer = new StringWriter();

		Map<String, Object> context = new HashMap<>();
		context.put("classDescription", classDescription);
		context.put("withHeaders", withHeaders); 

		template.evaluate(writer, context);

		return writer.toString();
	}

}
