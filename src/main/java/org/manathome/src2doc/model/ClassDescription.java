package org.manathome.src2doc.model;

import java.util.List;

/**
 * extracted java class metadata.
 *
 * @param name    java class name.
 * @param comment javadoc comment.
 * @param fields  fields in class.
 */
public record ClassDescription(String name, String comment, List<FieldDescription> fields) {

	/**
	 * ctor.
	 */
	public ClassDescription {
	}

}
