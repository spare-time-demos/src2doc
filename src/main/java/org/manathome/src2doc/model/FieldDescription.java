package org.manathome.src2doc.model;

/**
 * java class field metadata.
 *
 * @param name    field name.
 * @param comment javadoc field.
 * @param value   field constant value.
 */
public record FieldDescription(String name, String comment, String value) {

	/**
	 * .ctor
	 */
	public FieldDescription {
	}
}
