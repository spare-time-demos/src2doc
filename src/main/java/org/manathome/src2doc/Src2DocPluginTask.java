package org.manathome.src2doc;

import java.io.File;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

/** gradle build task. */
public class Src2DocPluginTask extends DefaultTask {

	public static final String SRC2DOC_TASK_NAME = "src2docGenerate";

	File sourceFile = null;
	
	File markupFile = null;

	boolean withHeaders = true;

	/** this output asciidoc file will be generated. */
	@OutputFile
	public File getMarkupFile() {
		return markupFile;
	}

	public void setMarkupFile(File markupFile) {
		this.markupFile = markupFile;
	}

	/** input a java source file. */
	public void setSourceFile(File sourceFile) {
    getLogger().debug("setting task SourceFile {}", sourceFile);
		this.sourceFile = sourceFile;
	}

	@InputFile
	public File getSourceFile() {
		return sourceFile;
	}

	@Input
	public boolean isWithHeaders() {
		return withHeaders;
	}

	public void setWithHeaders(boolean withHeaders) {
		this.withHeaders = withHeaders;
	}

	/** ctor. */
	public Src2DocPluginTask() {
		super();
		setGroup("documentation");
		setDescription("Parses java source files and produce asciidoc markup.");

		this.getLogger().debug("ctor " + SRC2DOC_TASK_NAME);
	}

	/** parse and generate. */
	@TaskAction
	public void src2doc() throws Exception {

    this.getLogger().info("generating markup from source..: {}", this.getSourceFile().getCanonicalPath());
    this.getLogger().info("                  into markup..: {}", this.getMarkupFile().getCanonicalFile());
		
		Src2DocProcessor s2d = new Src2DocProcessor();
		s2d.process(
				this.sourceFile, 
				this.markupFile,
				withHeaders);
	}

	
}
